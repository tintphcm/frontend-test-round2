import Vue from 'vue';
import Vuex from 'vuex';
import { taskList } from '../fakeData/task';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		user: null,
		project: null
	},
	getters: {},
	mutations: {
		SET_USER(state, user) {
			state.user = user;
		},
		SET_PROJECT(state, project) {
			state.project = project;
		}
	},
	actions: {
		async loadUser({ commit }) {
			let res = await JSON.parse(window.localStorage.getItem('@user'));
			console.log('res', res);
			commit('SET_USER', res === undefined || null ? taskList : res);
		},
		async loadProject({ commit }) {
			let res = await JSON.parse(window.localStorage.getItem('taskList'));
			console.log('res', res);
			commit('SET_PROJECT', res === undefined || null ? taskList : res);
		}
	},
	modules: {}
});
