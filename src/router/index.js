import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeView from '../views/HomeView.vue';
import LoginView from '../views/LoginView.vue';
import RegisterView from '../views/RegisterView.vue';

Vue.use(VueRouter);
// function getStateLogIn() {
// 	const isLoggedIn = setInterval(() => {
// 		window.localStorage.getItem('isLoggedIn');
// 	}, 1000);

// 	return isLoggedIn;
// }

const routes = [
	{
		path: '/',
		name: 'HomeView',
		component: HomeView
	},
	{
		path: '/login',
		name: 'LoginView',
		component: LoginView
	},
	{
		path: '/register',
		name: 'RegisterView',
		component: RegisterView
	}
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

export default router;
